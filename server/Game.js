
var Board = require('./Board.js');

function Game(board)
{

    if (!board instanceof Board) {
        throw new Error('Game expects an instance of Board');
    }

    this.state = 'not-started';
    this.players = {};
    this.playerCount = 0;
    this.board = board;
    this.playerTurn = null;
    this.drawn = false;
    this.winner = null;

    this.MAX_PLAYERS = 2;

    this.addPlayer = function(player)
    {

        if (this.players[player.getId()]) {
            throw new Error(this.ERROR_PLAYER_EXISTS);
        }

        if (this.playerCount >= this.MAX_PLAYERS) {
            return false;
        }

        if (!this.playerTurn) {
            this.playerTurn = player.getId();
        }

        this.players[player.getId()] = player;

        this.playerCount++;

        if (this.playerCount >= this.MAX_PLAYERS) {
            this.start();
            return true;
        }

        return false;
    };

    this.getPlayerIds = function()
    {
        return Object.keys(this.players);
    };

    this.hasPlayerWon = function()
    {
        var i, playerId, playerIds = this.getPlayerIds(), winner = null;
        for (i in playerIds) {
            playerId = playerIds[i];
            if (this.board.checkForWinConditions(playerId)) {
                winner = playerId;
            }
        }
        if (!winner && !board.hasMovesRemaining()) {
            this.drawn = true;
        }
        if (winner) {
            this.winner = this.getPlayerById(winner);
        }
        return winner || false;
    };

    this.getWinner = function()
    {
        return this.winner;
    };

    this.isDraw = function()
    {
        return this.drawn;
    };

    this.start = function()
    {
        this.state = 'in-progress';
    };

    this.finish = function()
    {
        this.state = 'finished';
    };


    this.hasStarted = function()
    {
        return this.state === 'in-progress' || this.state === 'finished';
    };

    this.getPlayerById = function(playerId)
    {
        return this.players[playerId] || null;
    };

    this.playMove = function(player, cell)
    {
        if (this.board.setCell(player.getId(), cell)) {
            this.switchTurns();
            return true;
        } else {
            return false;
        }
    };

    this.switchTurns = function()
    {
        var self = this, done = false;

        Object.keys(this.players).forEach(function(key) {
            if (!done && self.playerTurn !== key) {
                self.playerTurn = key;
                done = true;
            }
        });

    };

    this.getPlayerTurn = function()
    {
        return this.playerTurn;
    };

    this.getBoard = function()
    {
        return this.board;
    };

}

Game.prototype.ERROR_PLAYER_EXISTS = 'error-player-exists';


module.exports = Game;