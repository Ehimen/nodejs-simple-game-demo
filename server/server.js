

var io = require('socket.io').listen(8080),
    Board = require('./Board.js'),
    Game = require('./Game.js'),
    User = require('./User.js');

function MessageHandler(game)
{

    this.game = game;
    this.connections = [];

    this.processPlayerJoined = function(data)
    {
        this.validateProperty(data, 'playerId');

        this.sendSimple(data.playerId + ' has joined.');

        if (!this.game.hasStarted()) {
            try {

                if (this.game.addPlayer(new User(data.playerId))) {
                    // We're starting the game.
                    this.sendSimple('Game started with players: ' + this.game.getPlayerIds().join(' and '));
                    this.sendTurnStart();
                }

            } catch (err) {
                // Player already exist with this name.
                if (err.message === 'error-player-exists') {
                    this.sendSimple('Could not join. Player with name ' + data.playerId + ' already in game.');
                } else {
                    throw err;
                }
            }
        }

    };

    this.sendTurnStart = function()
    {
        var server = this;

        this.sendToAll({
            action: 'player-turn-start',
            data: {
                playerId: server.game.getPlayerTurn(),
                state: server.game.getBoard().getState()
            }
        });
    };

    this.addConnection = function(connection)
    {
        this.connections.push(connection);
    };

    this.sendSimple = function(message)
    {
        this.sendToAll({
            action: 'simple',
            data: message
        });
    };

    this.sendToAll = function(data)
    {
        this.connections.forEach(function(connection) {
            connection.emit('msg', data);
        });
    };

    this.processTurnPlayed = function(data)
    {
        var player;

        this.validateProperty(data, 'playerId');
        this.validateProperty(data, 'cell');

        player = this.game.getPlayerById(data.playerId);

        if (this.game.hasStarted()) {
            if (this.game.playMove(player, data.cell)) {
                if (this.game.hasPlayerWon()) {
                    this.winGame();
                } else {
                    if (this.game.isDraw()) {
                        this.drawGame();
                    } else {
                        this.sendTurnStart();
                    }
                }
            } else {
                this.sendSimple(data.playerId + ' tried to make an illegal move');
            }
        }
    };

    this.winGame = function()
    {
        var self = this;

        this.game.finish();
        this.sendToAll({
            action: 'finished',
            data: {
                winner: self.game.getWinner().getId(),
                state: self.game.getBoard().getState()
            }
        });
    };

    this.drawGame = function()
    {
        this.game.finish();
        this.sendToAll({
            action: 'finished',
            data: {
                winner: null,
                state: this.game.getBoard().getState()
            }
        });
    };

    this.processMessage = function(message)
    {
        this.validateMessage(message);

        if (message.action === 'player-joined') {
            this.processPlayerJoined(message.data);
        } else if (message.action === 'turn-played') {
            this.processTurnPlayed(message.data)
        }
    };

    this.validateMessage = function(message)
    {
        this.validateProperty(message);
        this.validateProperty(message, 'action');
    };

    this.validateProperty = function(object, property)
    {
        if (!object) {
            throw new Error('Invalid message received: no object found.')
        }
        if (property && !object[property]) {
            throw new Error('Invalid message received: expected ' + property + '.')
        }
    }
}


var board = new Board();
var game = new Game(board);
var messageHandler = new MessageHandler(game);


io.sockets.on('connection', function (socket) {


    if (!socket.isBound) {

        socket.on('msg', function(data) {
            messageHandler.processMessage(data);
        });

        messageHandler.addConnection(socket);

        socket.bound = true;
    }

});


