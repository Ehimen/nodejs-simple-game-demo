

module.exports = function Board()
{

    var i;

    this.cells = {};

    this.winPermutations = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
        [1, 4, 7],
        [2, 6, 8],
        [3, 6, 9],
        [1, 5, 9],
        [3, 5, 7]
    ];

    for (i = 1; i < 10; i++) {
        this.cells[i] = null;
    }

    this.checkForWinConditions = function(playerId)
    {
        var cells = this.cells,
            hasWon = false;
        this.winPermutations.forEach(function(p) {
            if (!hasWon) {
                hasWon = (cells[p[0]] === playerId
                    && cells[p[1]]  === playerId
                    && cells[p[2]]  === playerId);
            }
        });

        return hasWon;
    };

    this.setCell = function(playerId, cell)
    {
        if (!this.cells[cell] && cell >= 1 && cell <= 9) {
            this.cells[cell] = playerId;
            return true;
        }
        return false;
    };

    this.getState = function()
    {
        return this.cells;
    };

    this.hasMovesRemaining = function()
    {
        var isFinished = true, cells = this.cells;
        Object.keys(cells).forEach(function(index) {

            if (!cells[index]) {
                isFinished = false;
            }

        });
        return !isFinished;
    };

};