Tic Tac Toe
========================

Demo project written in NodeJS and Angular.

# Run demo

Clone this repository

```
npm install socket.io
```

```
node server/server.js
```


Direct a browser to client/index.html.

Direct a second browser tab to client/index.html