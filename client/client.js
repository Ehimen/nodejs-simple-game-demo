
angular.module('gameDemo', [])
    .controller('GameCtrl', GameCtrl)
    .factory('socket.io', function($window) {
        return $window.io;
    })
    .factory('jquery', function($window) {
        return $window.jQuery;
    });


function GameCtrl($scope, socketio, jQuery, $window)
{

    $scope.jQuery = jQuery;

    $scope.myTurn = false;

    $scope.state = [];

    $scope.finished = false;

    $scope.$on('socket.io-received', function(event, data) {

        var cssClassAdd,
            cssClassRemove;

        if (data.action === 'simple') {
            $scope.addNotification(data.data);
        }

        if (data.action === 'player-turn-start') {

            $scope.myTurn = data.data.playerId === $scope.name;

            $scope.addNotification('Start turn: ' + data.data.playerId);

            cssClassAdd = $scope.myTurn ? 'my-turn' : 'other-turn';
            cssClassRemove = $scope.myTurn ? 'other-turn' : 'my-turn';

            $scope.jQuery('.cell').each(function(i, e) {
                $scope.jQuery(e).addClass(cssClassAdd).removeClass(cssClassRemove);
            });
        }

        if (data.action === 'finished') {
            $scope.finishGame(data.data.winner)
        }

        if (data.data.state) {
            $scope.state = data.data.state;
        }

    });

    $scope.finishGame = function(winner)
    {
        $scope.finished = true;
        if (!winner) {
            $scope.addNotification('Game drawn! No winner');
        } else {
            $scope.addNotification(winner + ' won the game!');
        }

        $scope.jQuery('.overlay').removeClass('perm-hidden');
        $scope.jQuery('.cell').each(function(i, e) {
            $scope.jQuery(e).addClass('faded');
        });
    };

    $scope.takenClass = function(cellNumber)
    {
        return $scope.state[cellNumber] ? 'taken ' : '';
    };

    $scope.updateGameState = function(state)
    {
        $scope.jQuery('.cell').each(function(i, e) {
            var $e = $scope.jQuery(e);
            $e.html(state[$e.data('cell')]);
        });
    };

    $scope.notifications = [];

    $scope.socket = null;

    $scope.addNotification = function(msg)
    {
        $scope.notifications.unshift({text: msg});
    };

    $scope.getMessages = function()
    {
        return $scope.notifications;
    };

    $scope.init = function()
    {

        var socket = socketio.connect('http://127.0.0.1:8080');
        socket.on('msg', function(data) {
            $scope.$emit('socket.io-received', data);
            $scope.$apply();
        });
        $scope.socket = socket;

    };

    $scope.name = '';

    $scope.joinGame = function(event)
    {
        if (event.keyCode === 13) {
            if ($scope.name = event.target.value) {

                // Tell the server.
                $scope.socket.emit('msg', {
                    action: 'player-joined',
                    data: {
                        playerId: $scope.name
                    }
                });

                // Update UI.
                $scope.jQuery('.hidden').each(function(i, e) {
                    $scope.jQuery(e).removeClass('hidden');
                });
                $scope.jQuery('#name-input').html('Welcome, ' + $scope.name);
            }
        }
    };

    $scope.cellClick = function(event)
    {
        if ($scope.finished) {
            $scope.addNotification('The game is over!');
            return;
        }

        if ($scope.myTurn) {

            $scope.socket.emit('msg', {
                action: 'turn-played',
                data: {
                    playerId: $scope.name,
                    cell: $scope.jQuery(event.target).data('cell')
                }
            });


        } else {
            $scope.addNotification('It is not your turn');
        }
    };

}

GameCtrl.$inject = ['$scope', 'socket.io', 'jquery', '$window'];